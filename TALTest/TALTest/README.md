﻿## Synopsis

The project is a simple cover amount calculator used to calculate life insurance amount.
ASSUMPTIONS: 1. Clients can have a situation when they are neither have a mortgage or rent a property. 2. Once clients select mortgage/rent/have children/debt they must provide data (we use client side validation). 3. If the server side is not available we provide a message "Connection problems!", we don't show any technical details of the failure.
TODO: 1. Input controls must support formatting (currency on loose focus). 2. Once client side validation failed messages must show failed fields and provide details. 3. All technical messages (no connection message) must be displayed in a modal window. 4. Implement client side unit tests.

## Motivation

The project was createad as a test excercise in a course of hiring process for the TAL company.

## Installation

The project was developed using the visual studio 2015 Community Edition. After cloning from the bitbucket please click Rebuild All.

## Tests

The service layer was developed using the Web API 2 framework. TDD using the Approval testing framework was used for developing unit tests. Once the build process is successfully completed please open the Test Explorer and click Run All.

## Contributors

Anton Anokhin.

## License

It is completely free.
