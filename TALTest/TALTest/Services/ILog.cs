﻿using System;

namespace TALTest.Services
{
    /// <summary>
    /// Provides logic for login exceptions.
    /// </summary>
    public interface ILog
    {
        /// <summary>
        /// Writes exceptions into event log.
        /// </summary>
        /// <param name="ex">Represent an exeption.</param>
        void WriteException(Exception ex);

        /// <summary>
        /// Writes information into event log.
        /// </summary>
        /// <param name="msg">Messge.</param>
        void WriteInfo(string msg);
    }
}
