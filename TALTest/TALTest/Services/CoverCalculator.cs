﻿using System;
using TALTest.Models;

namespace TALTest.Services
{
    /// <summary>
    /// Cover Amount caclculator service.
    /// </summary>
    public class CoverCalculator : ICalculator
    {
        /// <summary>
        /// Logger
        /// </summary>
        private readonly ILog log;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log">Logger</param>
        public CoverCalculator(ILog log)
        {
            this.log = log;
        }

        /// <summary>
        /// Calculate cover amount.
        /// </summary>
        /// <param name="personal">Personal details.</param>
        /// <returns>Calculated cover amount.</returns>
        public double Calculate(PersonalDetailsType personal)
        {
            log.WriteInfo(string.Format("Person's details: Mortgage = {0}, Rent = {1}, Children = {2}, Debt = {3}", 
                personal.Mortgage, personal.Rent, personal.Children, personal.Debt));

            return (personal.Mortgage == 0 ? personal.Rent * 12 * 20 : personal.Mortgage)
                + (personal.Children * 50000) + personal.Debt;
        }
    }
}