﻿using System;
using Diag = System.Diagnostics;

namespace TALTest.Services
{
    /// <summary>
    /// Provides logic for login exceptions.
    /// </summary>
    public class EventLog
        : ILog
    {
        const string SourceName = "Enterprise.TAL";
        const string LogName = "Application";

        /// <summary>
        /// Constructor sets up event log.
        /// </summary>
        public EventLog()
        {
            if (!Diag.EventLog.SourceExists(SourceName))
                Diag.EventLog.CreateEventSource(SourceName, LogName);
        }

        /// <summary>
        /// Writes exceptions into event log.
        /// </summary>
        /// <param name="ex">Represent an exeption.</param>
        public void WriteException(Exception ex)
        {
            var msg = String.Format("{0}: {1}", ex.GetType().FullName, ex.Message);
            Diag.EventLog.WriteEntry(SourceName, msg);
        }

        /// <summary>
        /// Writes information into event log.
        /// </summary>
        /// <param name="msg">Messge.</param>
        public void WriteInfo(string msg)
        {
            Diag.EventLog.WriteEntry(SourceName, msg);
        }
    }
}