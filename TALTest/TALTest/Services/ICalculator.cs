using TALTest.Models;

namespace TALTest.Services
{
    /// <summary>
    /// Calculators.
    /// </summary>
    public interface ICalculator
    {
        /// <summary>
        /// Make calculation
        /// </summary>
        /// <param name="personal">Personal details.</param>
        /// <returns>Calculated number.</returns>
        double Calculate(PersonalDetailsType personal);
    }
}