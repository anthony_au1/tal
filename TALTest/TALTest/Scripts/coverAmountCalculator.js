﻿var coverCalcApp = angular.
    module('coverCalcApp', ['ngResource']);

coverCalcApp.
    constant('calculatorURL', '/api/coveramount');


coverCalcApp.
    service('calcService', ['$q', '$resource', 'calculatorURL',
        function ($q, $resource, calculatorURL) {
            var r = $resource(calculatorURL, null, {
                'getCoverAmount': { method: 'POST' }
            });

            return {
                resetCalc: function(scope) {
                    scope.personal = {
                        mortgage: 0,
                        rent: 0,
                        children: 0,
                        debt: 0
                    };
                    scope.children = 'no';
                    scope.debt = 'no';
                    scope.state = 0;

                    scope.result = null;

                    if (angular.isDefined(scope.mortgageOrRent))
                        delete scope.mortgageOrRent;
                },
                calcAmount: function (personal) {
                    var deferred = $q.defer();
                    r.getCoverAmount(personal,
                        function(resp, headers) {
                            deferred.resolve(resp);
                        },
                        function(err) {
                            deferred.reject({
                                status: 0,
                                reason: 'Connections problems!',
                                coverAmount: 0
                            });
                        });

                    return deferred.promise;
                }
            };
        }
    ]);

coverCalcApp.
    controller('CalcCtrl', ['$scope', 'calcService',
        function ($scope, calcService) {
            calcService.resetCalc($scope);

            $scope.calc = function (personal) {
                calcService.calcAmount(personal).
                    then(function (data) {
                        $scope.result = data;
                    },
                    function (data) {
                        $scope.result = data;
                    });
                $scope.state = 1;
            };

            $scope.repeat = function() {
                calcService.resetCalc($scope);
                $scope.state = 0;
            };
        }]);