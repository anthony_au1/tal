﻿
namespace TALTest.Models
{
    /// <summary>
    /// Personal information.
    /// </summary>
    public class PersonalDetailsType
    {
        /// <summary>
        /// Mortgage amount.
        /// </summary>
        public double Mortgage { get; set; }

        /// <summary>
        /// Monthly rent payments.
        /// </summary>
        public double Rent { get; set; }

        /// <summary>
        /// Number of dependent children.
        /// </summary>
        public int Children { get; set; }

        /// <summary>
        /// Outstanding debt.
        /// </summary>
        public double Debt { get; set; }
    }
}