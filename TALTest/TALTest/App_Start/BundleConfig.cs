﻿using System.Web.Optimization;

namespace TALTest
{
    /// <summary>
    /// Bundling configuration
    /// </summary>
    public class BundleConfig
    {
        /// <summary>
        /// Registers underlying bundles.
        /// </summary>
        /// <param name="bundles">Contains and manages the set of registered System.Web.Optimization.Bundle objects in an ASP.NET application.</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/angular.*"));

            bundles.Add(new ScriptBundle("~/bundles/angular-resource").Include(
                "~/Scripts/angular-resource.*"));

            bundles.Add(new ScriptBundle("~/bundles/coverAmountCalculator").Include(
                        "~/Scripts/coverAmountCalculator.*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}