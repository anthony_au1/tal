﻿using System.Net.Http.Formatting;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Newtonsoft.Json.Serialization;

namespace TALTest
{
    /// <summary>
    /// WEB API configuration
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Registers underlying components.
        /// </summary>
        /// <param name="config">Represents a configuration of System.Web.Http.HttpServer instances.</param>
        /// <param name="scope">An Autofac.ILifetimeScope tracks the instantiation of component instances.
        /// It defines a boundary in which instances are shared and configured.  Disposing
        /// an Autofac.ILifetimeScope will dispose the components that were resolved
        /// through it.</param>
        public static void Register(HttpConfiguration config, ILifetimeScope scope)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter()
            {
                SerializerSettings =
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }
            });

            config.DependencyResolver = new AutofacWebApiDependencyResolver(scope);
        }

        /// <summary>
        /// Registers underlying components.
        /// </summary>
        /// <param name="config">Represents a configuration of System.Web.Http.HttpServer instances.</param>
        public static void Register(HttpConfiguration config)
        {
            var container = DIConfig.RegisterDependencies(config).Build();
            Register(config, container);
        }
    }
}
