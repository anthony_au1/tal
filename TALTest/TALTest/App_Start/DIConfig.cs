﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using TALTest.Services;

namespace TALTest
{
    /// <summary>
    /// Provides dependencies binding config.
    /// </summary>
    public static class DIConfig
    {
        /// <summary>
        /// Registers all dependencies. 
        /// </summary>
        /// <param name="config">Represents a configuration of System.Web.Http.HttpServer instances.</param>
        /// <returns>A builder object used in dependency resolver.</returns>
        public static ContainerBuilder RegisterDependencies(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder
                .RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterWebApiFilterProvider(config);

            builder
                .RegisterType<CoverCalculator>()
                .As<ICalculator>();

            builder
                .RegisterType<EventLog>()
                .As<ILog>();

            return builder;
        }
    }
}