﻿using System.Web.Http;
using TALTest.Models;
using TALTest.Services;

namespace TALTest.Controllers
{
    /// <summary>
    /// TAL Cover Amount Calculator controller.
    /// </summary>
    [RoutePrefix("api")]
    public class CalculatorController : ApiController
    {
        /// <summary>
        /// Cover Amount Calculator
        /// </summary>
        private readonly ICalculator calc;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="calc">Cover Amount Calculator</param>
        public CalculatorController(ICalculator calc)
        {
            this.calc = calc;
        }

        // POST: api/Calculator
        [Route("coveramount")]
        public object Post(PersonalDetailsType personal)
        {
            var coverAmount = calc.Calculate(personal);

            var status = coverAmount >= 50000 && coverAmount <= 1000000 ? 1 : 0;

            return new
            {
                Status = status,
                Reason = status == 0 ? string.Format("The cover amount range allowed is between $50,000 - $1,000,000, the currnt amount is {0:C2}", coverAmount) : default(string),
                CoverAmount = coverAmount
            };
        }
    }
}
