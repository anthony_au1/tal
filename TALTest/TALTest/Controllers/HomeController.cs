﻿using System.Web.Mvc;

namespace TALTest.Controllers
{
    /// <summary>
    /// Default/Home page controller.
    /// </summary>
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}
