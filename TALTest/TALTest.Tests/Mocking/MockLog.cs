﻿using System;
using System.Diagnostics;
using TALTest.Services;

namespace TALTest.Tests.Mocking
{
    /// <summary>
    /// Provides logic for login exceptions.
    /// </summary>
    public class MockLog
        : ILog
    {
        /// <summary>
        /// Writes exceptions into event log.
        /// </summary>
        /// <param name="ex">Represent an exeption.</param>
        public void WriteException(Exception ex)
        {
            var msg = String.Format("{0}: {1}", ex.GetType().FullName, ex.Message);
            Debug.WriteLine(msg);
        }

        /// <summary>
        /// Writes information into event log.
        /// </summary>
        /// <param name="msg">Messge.</param>
        public void WriteInfo(string msg)
        {
            Debug.WriteLine(msg);
        }
    }
}
