﻿using ApprovalTests;
using ApprovalTests.Reporters;
using System.Net;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TALTest.Models;

namespace TALTest.Tests
{
    [TestClass]
    [UseReporter(typeof(DiffReporter))]
    public class CoverAmountCalcTests
    {
        [TestMethod]
        public void Calculate_Cover_Amount_Mortgage_No_Children_No_Debt()
        {
            // arrange
            using (var context = Host.Run())
            {
                var personal = new PersonalDetailsType()
                {
                    Mortgage = 500000d
                };

                // act
                var response = context.Client.PostAsJsonAsync("api/coveramount", personal).Result;

                // assert
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var json = response.Content.ReadAsStringAsync().Result;
                Approvals.VerifyJson(json);
            }
        }

        [TestMethod]
        public void Calculate_Cover_Amount_Rent_Children_Debt()
        {
            // arrange
            using (var context = Host.Run())
            {
                var personal = new PersonalDetailsType()
                {
                    Rent = 1800d,
                    Children = 2,
                    Debt = 50000d
                };

                // act
                var response = context.Client.PostAsJsonAsync("api/coveramount", personal).Result;

                // assert
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var json = response.Content.ReadAsStringAsync().Result;
                Approvals.VerifyJson(json);
            }
        }

        [TestMethod]
        public void Calculate_Cover_Amount_Less_Than_Minimum()
        {
            // arrange
            using (var context = Host.Run())
            {
                var personal = new PersonalDetailsType()
                {
                    Rent = 0d
                };

                // act
                var response = context.Client.PostAsJsonAsync("api/coveramount", personal).Result;

                // assert
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var json = response.Content.ReadAsStringAsync().Result;
                Approvals.VerifyJson(json);
            }
        }

        [TestMethod]
        public void Calculate_Cover_Amount_Greater_Than_Maximum()
        {
            // arrange
            using (var context = Host.Run())
            {
                var personal = new PersonalDetailsType()
                {
                    Mortgage = 1200000d
                };

                // act
                var response = context.Client.PostAsJsonAsync("api/coveramount", personal).Result;

                // assert
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var json = response.Content.ReadAsStringAsync().Result;
                Approvals.VerifyJson(json);
            }
        }

        [TestMethod]
        public void Calculate_Cover_Amount_Mortgage_And_Rent()
        {
            // arrange
            using (var context = Host.Run())
            {
                var personal = new PersonalDetailsType()
                {
                    Mortgage = 300000d,
                    Rent = 1800d
                };

                // act
                var response = context.Client.PostAsJsonAsync("api/coveramount", personal).Result;

                // assert
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var json = response.Content.ReadAsStringAsync().Result;
                Approvals.VerifyJson(json);
            }
        }
    }
}
