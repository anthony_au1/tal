﻿using System;
using System.Net.Http;
using System.Web.Http;
using Autofac;
using Microsoft.Owin.Hosting;
using Owin;
using TALTest.Services;
using TALTest.Tests.Mocking;

namespace TALTest.Tests
{
    public class Host
    {
        public static RunContext Run()
        {
            var baseUri = new Uri("http://localhost:1337/");
            var web = WebApp.Start(baseUri.ToString(), app =>
            {
                var config = new HttpConfiguration();

                var builder = DIConfig.RegisterDependencies(config);

                builder
                  .RegisterType<MockLog>()
                  .As<ILog>();

                WebApiConfig.Register(config, builder.Build());

                app.UseWebApi(config);
            });

            Action cleanup = () =>
            {
                web.Dispose();
            };

            return new RunContext(cleanup) { Client = new HttpClient { BaseAddress = baseUri } };
        }

        public class RunContext
            : IDisposable
        {
            private readonly Action cleanup;

            public HttpClient Client { get; set; }

            public RunContext(Action cleanup)
            {
                this.cleanup = cleanup;
            }

            public void Dispose()
            {
                cleanup();
            }
        }
    }
}
